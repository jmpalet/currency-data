<?php

namespace App\Repository;

use App\Entity\Rate;
use App\Exception\RepositoryException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    /**
     * RateRepository constructor.
     *
     * @param RegistryInterface $registry
     * @codeCoverageIgnore
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    /**
     * @param string $fromCurrencyCode
     * @param string $toCurrencyCode
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     *
     * @codeCoverageIgnore
     */
    public function getOneByCurrencyCodes(string $fromCurrencyCode, string $toCurrencyCode)
    {
        return $this->createQueryBuilder('r')
            ->innerJoin('r.fromCurrency', 'c_from')
            ->innerJoin('r.toCurrency', 'c_to')
            ->addSelect('c_from')
            ->addSelect('c_to')
            ->andWhere('c_from.code = :from_code')
            ->andWhere('c_to.code = :to_code')
            ->setParameters([
                'from_code' => $fromCurrencyCode,
                'to_code' => $toCurrencyCode,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Rate $rate
     *
     * @return Rate
     *
     * @throws RepositoryException
     */
    public function save(Rate $rate): Rate
    {
        try {
            if ($saved = $this->getOneByCurrencyCodes($rate->getFromCurrency()->getCode(), $rate->getToCurrency()->getCode())) {
                $rate = $saved->setRate($rate->getRate());
            }
        } catch (NonUniqueResultException $e) {
            throw new RepositoryException('Error fetching currency by codes', $e);
        }

        try {
            $this->getEntityManager()->persist($rate);
            $this->getEntityManager()->flush();
        } catch (OptimisticLockException | ORMException $e) {
            throw new RepositoryException('Error persisting entity', $e);
        }

        return $rate;
    }
}
