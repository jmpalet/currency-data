<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Rate;
use App\Exception\RepositoryException;
use App\Repository\CurrencyRepository;
use App\Repository\RateRepository;

class RateService
{
    /**
     * @var RateRepository
     */
    private $rateRepository;
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    public function __construct(RateRepository $rateRepository, CurrencyRepository $currencyRepository)
    {
        $this->rateRepository = $rateRepository;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param Rate $rate
     *
     * @return Rate|null
     */
    public function save(Rate $rate): ?Rate
    {
        if ($from = $this->currencyRepository->findOneBy(['code' => $rate->getFromCurrency()->getCode()])) {
            $rate->setFromCurrency($from);
        }

        if ($to = $this->currencyRepository->findOneBy(['code' => $rate->getToCurrency()->getCode()])) {
            $rate->setToCurrency($to);
        }

        try {
            return $this->rateRepository->save($rate);
        } catch (RepositoryException $exception) {
            return null;
        }
    }

    /**
     * @return Rate[] array
     * @codeCoverageIgnore
     */
    public function getRates(): array
    {
        return $this->rateRepository->findAll();
    }
}
