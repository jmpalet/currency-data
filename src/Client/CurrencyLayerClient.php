<?php

declare(strict_types=1);

namespace App\Client;

use App\Contract\ApiClient;
use App\Contract\RateTransformer;
use App\Exception\GetRatesFailedException;
use App\Exception\TransformFailedException;
use App\View\Model\ApiQuote;
use Assert\Assert;
use InvalidArgumentException;
use OceanApplications\currencylayer\client;

class CurrencyLayerClient implements ApiClient
{
    /**
     * @var client
     */
    private $client;
    /**
     * @var RateTransformer
     */
    private $rateTransformer;

    public function __construct(client $client, RateTransformer $rateTransformer)
    {
        $this->client = $client;
        $this->rateTransformer = $rateTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function getRates(string $source, array $targets): array
    {
        try {
            $response = $this->client->source($source)->currencies(implode(',', $targets))->live();
        } catch (InvalidArgumentException $exception) {
            throw new GetRatesFailedException($exception->getMessage(), $exception);
        }

        if (!isset($response['success']) || !$response['success']) {
            throw new GetRatesFailedException('Unsuccessful response');
        }

        if (!isset($response['quotes']) || empty($response['quotes'])) {
            throw new GetRatesFailedException('Quotes not found');
        }

        $rates = [];
        foreach ($response['quotes'] as $key => $quote) {
            $quote = new ApiQuote($key, $quote);
            try {
                $rate = $this->rateTransformer->api2entity($quote);
                Assert::that($rate->getFromCurrency()->getCode())->eq($source,
                    sprintf('Wrong source currencies: %s does not match %s', $source, $rate->getFromCurrency()->getCode()));
                $rates[] = $rate;
            } catch (TransformFailedException $exception) {
                throw new GetRatesFailedException('Transform exception', $exception);
            }
        }

        return $rates;
    }
}
