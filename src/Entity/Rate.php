<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RateRepository")
 * @ORM\Table(name="rates",uniqueConstraints={@ORM\UniqueConstraint(name="unique_rate",columns={"from_currency_id","to_currency_id"})})
 */
class Rate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, name="from_currency_id", referencedColumnName="id")
     */
    private $fromCurrency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, name="to_currency_id", referencedColumnName="id")
     */
    private $toCurrency;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=6)
     */
    private $rate;

    public function __construct(
        Currency $fromCurrency,
        Currency $toCurrency,
        float $rate
    ) {
        $this->fromCurrency = $fromCurrency;
        $this->toCurrency = $toCurrency;
        $this->rate = $rate;
    }

    public function getFromCurrency(): Currency
    {
        return $this->fromCurrency;
    }

    public function getToCurrency(): Currency
    {
        return $this->toCurrency;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function setRate($rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @param mixed $fromCurrency
     */
    public function setFromCurrency($fromCurrency): void
    {
        $this->fromCurrency = $fromCurrency;
    }

    /**
     * @param mixed $toCurrency
     */
    public function setToCurrency($toCurrency): void
    {
        $this->toCurrency = $toCurrency;
    }
}
