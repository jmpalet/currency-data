<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813132509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rates (id INT AUTO_INCREMENT NOT NULL, from_currency_id INT NOT NULL, to_currency_id INT NOT NULL, rate NUMERIC(12, 6) NOT NULL, INDEX IDX_44D4AB3CA66BB013 (from_currency_id), INDEX IDX_44D4AB3C16B7BF15 (to_currency_id), UNIQUE INDEX unique_rate (from_currency_id, to_currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currencies (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_37C4469377153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rates ADD CONSTRAINT FK_44D4AB3CA66BB013 FOREIGN KEY (from_currency_id) REFERENCES currencies (id)');
        $this->addSql('ALTER TABLE rates ADD CONSTRAINT FK_44D4AB3C16B7BF15 FOREIGN KEY (to_currency_id) REFERENCES currencies (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rates DROP FOREIGN KEY FK_44D4AB3CA66BB013');
        $this->addSql('ALTER TABLE rates DROP FOREIGN KEY FK_44D4AB3C16B7BF15');
        $this->addSql('DROP TABLE rates');
        $this->addSql('DROP TABLE currencies');
    }
}
