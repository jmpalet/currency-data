<?php

declare(strict_types=1);

namespace App\Command;

use App\Contract\ApiClient;
use App\Entity\Rate;
use App\Exception\GetRatesFailedException;
use App\Service\RateService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateRatesCommand extends Command
{
    protected static $defaultName = 'app:update-rates';

    /** @var ApiClient */
    private $client;
    /**
     * @var RateService
     */
    private $rateService;

    /**
     * UpdateRatesCommand constructor.
     *
     * @param ApiClient   $client
     * @param RateService $rateService
     * @codeCoverageIgnore
     */
    public function __construct(ApiClient $client, RateService $rateService)
    {
        parent::__construct();
        $this->client = $client;
        $this->rateService = $rateService;
    }

    /**
     * @codeCoverageIgnore
     */
    protected function configure()
    {
        $this
            ->setDescription('Updates rates in the database')
            ->addArgument('source', InputArgument::REQUIRED, 'Source currency')
            ->addArgument('target', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Target currency(ies)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Updating rates',
            '============',
            '',
        ]);

        try {
            $rates = $this->client->getRates(
                $input->getArgument('source'),
                $input->getArgument('target')
            );

            foreach ($rates as $key => $rate) {
                $rates[$key] = $this->rateService->save($rate);
            }

            $table = new Table($output);
            $table->setHeaders(['From', 'To', 'Rate']);
            $table->setRows(array_map(static function (Rate $rate) {
                return [
                    $rate->getFromCurrency()->getCode(),
                    $rate->getToCurrency()->getCode(),
                    (string) $rate->getRate(),
                ];
            }, $rates));

            $table->render();
            $output->writeln('Done!');
        } catch (GetRatesFailedException $exception) {
            $output->writeln(sprintf('<error>Error fetching rates: %s</error>', $exception->getMessage()));
        }
    }
}
