<?php

declare(strict_types=1);

namespace App\View\Model;

class Rate
{
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $to;
    /**
     * @var string
     */
    private $rate;

    public function __construct(
        string $from,
        string $to,
        string $rate
    ) {
        $this->from = $from;
        $this->to = $to;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getRate(): string
    {
        return $this->rate;
    }
}
