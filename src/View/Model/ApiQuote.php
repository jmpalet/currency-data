<?php

declare(strict_types=1);

namespace App\View\Model;

class ApiQuote
{
    public const KEY_FORMAT = '/^([A-Z]{3})([A-Z]{3})$/';

    /**
     * @var string
     */
    private $key;
    /**
     * @var float
     */
    private $quote;

    public function __construct(
        string $key,
        float $quote
    ) {
        $this->key = $key;
        $this->quote = $quote;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return float
     */
    public function getQuote(): float
    {
        return $this->quote;
    }
}
