<?php

declare(strict_types=1);

namespace App\View\Transformer;

use App\Contract\RateTransformer as Contract;
use App\Entity\Currency;
use App\Entity\Rate as EntityModel;
use App\Exception\TransformFailedException;
use App\View\Model\ApiQuote;
use App\View\Model\Rate as ViewModel;

class RateTransformer implements Contract
{
    /**
     * @param EntityModel $entity
     *
     * @return ViewModel
     */
    public function entity2view(EntityModel $entity): ViewModel
    {
        return new ViewModel(
            $entity->getFromCurrency()->getCode(),
            $entity->getToCurrency()->getCode(),
            (string) $entity->getRate()
        );
    }

    /**
     * @param ApiQuote $quote
     *
     * @return EntityModel
     *
     * @throws TransformFailedException
     */
    public function api2entity(ApiQuote $quote): EntityModel
    {
        if (!preg_match(ApiQuote::KEY_FORMAT, $quote->getKey(), $matches)) {
            throw new TransformFailedException(sprintf('Invalid format for key %s', $quote->getKey()));
        }

        return new EntityModel(
            new Currency($matches[1]),
            new Currency($matches[2]),
            $quote->getQuote()
        );
    }
}
