<?php

declare(strict_types=1);

namespace App\Controller;

use App\Contract\RateTransformer;
use App\Entity\Rate;
use App\Service\RateService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class RateController extends AbstractFOSRestController
{
    /**
     * @var RateService
     */
    private $rateService;
    /**
     * @var RateTransformer
     */
    private $rateTransformer;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        RateService $rateService,
        RateTransformer $rateTransformer,
        SerializerInterface $serializer
    ) {
        $this->rateService = $rateService;
        $this->rateTransformer = $rateTransformer;
        $this->serializer = $serializer;
    }

    /**
     * @Rest\Get("/rates")
     *
     * @return JsonResponse
     */
    public function getRates(): JsonResponse
    {
        $view = array_map(
            function (Rate $rate) {
                return $this->rateTransformer->entity2view($rate);
            }, $this->rateService->getRates());

        return JsonResponse::fromJsonString($this->serializer->serialize($view, 'json'));
    }
}
