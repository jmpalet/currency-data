<?php

declare(strict_types=1);

namespace App\Factory;

use OceanApplications\currencylayer\client;

class ClientFactory
{
    /**
     * @param string $key
     *
     * @return client
     * @codeCoverageIgnore
     */
    public static function create(string $key): client
    {
        return new client($key);
    }
}
