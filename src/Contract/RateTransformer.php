<?php

declare(strict_types=1);

namespace App\Contract;

use App\Entity\Rate as EntityModel;
use App\Exception\TransformFailedException;
use App\View\Model\ApiQuote;
use App\View\Model\Rate as ViewModel;

interface RateTransformer
{
    /**
     * @param EntityModel $entity
     *
     * @return ViewModel
     */
    public function entity2view(EntityModel $entity): ViewModel;

    /**
     * @param ApiQuote $quote
     *
     * @return EntityModel
     *
     * @throws TransformFailedException
     */
    public function api2entity(ApiQuote $quote): EntityModel;
}
