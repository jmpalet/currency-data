<?php

declare(strict_types=1);

namespace App\Contract;

use App\Exception\GetRatesFailedException;

interface ApiClient
{
    /**
     * @param string $source
     * @param array  $targets
     *
     * @return array
     *
     * @throws GetRatesFailedException
     */
    public function getRates(string $source, array $targets): array;
}
