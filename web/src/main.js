import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Axios from 'axios'
import BootstrapVue from 'bootstrap-vue'

Vue.prototype.$http = Axios;
Vue.config.productionTip = false;
Vue.use(BootstrapVue);

new Vue({
  router,
  render: function(h) {
    return h(App);
  }
}).$mount("#app");

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
