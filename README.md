## Development
#### Start the app:
```
docker-compose up --build -d
docker-compose exec php bin/console doctrine:database:create --if-not-exists --no-interaction
docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction
```
#### Update rates
```
docker-compose exec php bin/console app:update-rates {SOURCE} {TARGET}
```
where:
* `SOURCE` is the code of the source currency
* `TARGET` are the target currency codes (separated by space)

Example:

```docker-compose exec php bin/console app:update-rates USD EUR CHF```

would update rates from `USD` to `EUR` and `CHF`

#### Run Unit tests:
```
docker-compose exec php vendor/bin/phpunit
```
#### Run CS Fixer:
```
docker-compose exec php vendor/bin/php-cs-fixer fix
```
#### Ports:
* `8001`: Frontend
* `8008`: Api
* `33306`: MySQL
* `9000`: Xdebug
