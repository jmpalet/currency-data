<?php

declare(strict_types=1);

namespace App\Tests\Support;

use function array_key_exists;
use function array_map;
use InvalidArgumentException;
use function is_string;
use Prophecy\Prophecy\ObjectProphecy;

trait DependencyMocking
{
    /**
     * @var ObjectProphecy[]
     */
    protected $dependencies = [];

    /**
     * @param string ...$dependencies
     */
    protected function mockServiceDependencies(string ...$dependencies): void
    {
        foreach ($dependencies as $dependency) {
            $this->mockServiceDependency($dependency);
        }
    }

    /**
     * @param string $class
     * @param string ...$dependendies
     *
     * @return object
     */
    protected function constructWithMockedDependencies(string $class, string ...$dependendies)
    {
        $this->mockServiceDependencies(...$dependendies);

        return new $class(
            ...$this->getServiceDependencies(...$dependendies)
        );
    }

    /**
     * @param string $class
     * @param mixed  ...$dependencies
     *
     * @return mixed
     */
    protected function constructWithMixedDependencies(string $class, ...$dependencies)
    {
        $arguments = array_map(function ($dependency) {
            if (is_string($dependency)) {
                $this->mockServiceDependency($dependency);

                return $this->getServiceDependency($dependency);
            }

            return $dependency;
        }, $dependencies);

        return new $class(...$arguments);
    }

    /**
     * @param string $name
     *
     * @return ObjectProphecy
     */
    protected function getServiceDependencyMock(string $name): ObjectProphecy
    {
        $dependency = $this->dependencies[$name] ?? false;

        if (false === $dependency) {
            throw new InvalidArgumentException(
                sprintf("No Dependency '%s' found.", $name)
            );
        }

        return $dependency;
    }

    /**
     * @param string $name
     *
     * @return object
     */
    protected function getServiceDependency(string $name)
    {
        return $this->getServiceDependencyMock($name)->reveal();
    }

    /**
     * @param string[] $dependencies
     *
     * @return array
     */
    protected function getServiceDependencies(string ...$dependencies): array
    {
        return array_map(
            function (string $class) {
                return $this->getServiceDependency($class);
            },
            $dependencies
        );
    }

    /**
     * @param string $dependency
     *
     * @return ObjectProphecy
     */
    protected function mockServiceDependency(string $dependency): ObjectProphecy
    {
        if (!array_key_exists($dependency, $this->dependencies)) {
            $this->dependencies[$dependency] = $this->prophesize($dependency);
        }

        return $this->dependencies[$dependency];
    }
}
