<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Currency;
use App\Entity\Rate;
use App\Exception\RepositoryException;
use App\Repository\CurrencyRepository;
use App\Repository\RateRepository;
use App\Service\RateService;
use App\Tests\Support\DependencyMocking;
use Assert\Assert;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class RateServiceTest extends TestCase
{
    use DependencyMocking;

    /** @var RateService */
    private $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = $this->constructWithMixedDependencies(
            RateService::class,

            RateRepository::class,
            CurrencyRepository::class
        );
    }

    /**
     * @dataProvider saveTestDataProvider
     *
     * @param Rate $rate
     * @param $fromCurrency
     * @param $toCurrency
     * @param $expected
     * @param null $exception
     */
    public function testSave(Rate $rate, $fromCurrency, $toCurrency, $expected, $exception = null)
    {
        $this->getServiceDependencyMock(CurrencyRepository::class)
            ->findOneBy(Argument::type('array'))
            ->will(function ($args) use ($rate, $fromCurrency, $toCurrency) {
                $code = $args[0]['code'] ?? null;
                Assert::that($code)->string();
                if ($fromCurrency && $code === $rate->getFromCurrency()->getCode()) {
                    return $fromCurrency;
                }
                if ($toCurrency && $code === $rate->getToCurrency()->getCode()) {
                    return $toCurrency;
                }

                return null;
            });

        if ($exception) {
            $this->getServiceDependencyMock(RateRepository::class)
                ->save(Argument::type(Rate::class))
                ->willThrow($exception);
        } else {
            $this->getServiceDependencyMock(RateRepository::class)
                ->save(Argument::type(Rate::class))
                ->willReturn($rate);
        }

        $actual = $this->service->save($rate);
        $this->assertEquals($actual, $expected);
    }

    public function saveTestDataProvider()
    {
        yield 'Save rate - new currencies' => [
            $rate = new Rate(new Currency('EUR'), new Currency('USD'), 1.2),
            false,
            false,
            $rate,
        ];

        yield 'Save rate - from currency exists' => [
            $rate,
            new Currency('EUR'),
            false,
            $rate,
        ];

        yield 'Save rate - to currency exists' => [
            $rate,
            false,
            new Currency('USD'),
            $rate,
        ];

        yield 'Repository exception' => [
            $rate,
            false,
            false,
            null,
            RepositoryException::class,
        ];
    }
}
