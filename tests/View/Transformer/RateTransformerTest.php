<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Currency;
use App\Entity\Rate;
use App\Exception\TransformFailedException;
use App\View\Model\ApiQuote;
use App\View\Transformer\RateTransformer;
use PHPUnit\Framework\TestCase;

class RateTransformerTest extends TestCase
{
    /**
     * @dataProvider api2entityDataProvider
     */
    public function testApi2Entity($quote, $expected, $exception = null)
    {
        if ($exception) {
            $this->expectException($exception);
        }

        $actual = (new RateTransformer())->api2entity($quote);
        $this->assertEquals($expected, $actual);
    }

    public function api2entityDataProvider()
    {
        yield 'Valid transformation' => [
            new ApiQuote('EURUSD', 1.0),
            new Rate(new Currency('EUR'), new Currency('USD'), 1.0),
        ];

        yield 'wrong key' => [
            new ApiQuote('WRONG_KEY', 1.0),
            null,
            TransformFailedException::class,
        ];
    }
}
