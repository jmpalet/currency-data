<?php

declare(strict_types=1);

use App\Controller\RateController;
use App\Entity\Currency;
use App\Entity\Rate as Model;
use App\Service\RateService;
use App\Tests\Support\DependencyMocking;
use App\View\Model\Rate as View;
use App\View\Transformer\RateTransformer;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class RateControllerTest extends TestCase
{
    use DependencyMocking;

    /** @var RateController */
    private $controller;

    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = JMS\Serializer\SerializerBuilder::create()->build();

        parent::setUp();
        $this->controller = $this->constructWithMixedDependencies(
            RateController::class,

            RateService::class,
            new RateTransformer(),
            $this->serializer
        );
    }

    /**
     * @dataProvider getRatesDataProvider
     *
     * @param $rates
     * @param $expected
     */
    public function testGetRates(
        $rates,
        $expected
    ) {
        $this->getServiceDependencyMock(RateService::class)
            ->getRates()
            ->willReturn($rates);

        $actual = $this->controller->getRates();

        $this->assertEquals(
            JsonResponse::fromJsonString($this->serializer->serialize($expected, 'json')),
            $actual
        );
    }

    public function getRatesDataProvider()
    {
        yield 'Valid response' => [
            [
                new Model(new Currency('EUR'), new Currency('USD'), 1.123),
                new Model(new Currency('EUR'), new Currency('CHF'), 1.321),
            ],
            [
                new View('EUR', 'USD', '1.123'),
                new View('EUR', 'CHF', '1.321'),
            ],
        ];

        yield 'Empty response' => [
            [],
            [],
        ];
    }
}
