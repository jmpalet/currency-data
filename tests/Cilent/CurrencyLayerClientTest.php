<?php

declare(strict_types=1);

use App\Client\CurrencyLayerClient;
use App\Entity\Currency;
use App\Entity\Rate;
use App\Exception\GetRatesFailedException;
use App\Exception\TransformFailedException;
use App\Tests\Support\DependencyMocking;
use App\View\Model\ApiQuote;
use App\View\Transformer\RateTransformer;
use Assert\Assert;
use Assert\InvalidArgumentException;
use OceanApplications\currencylayer\client;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CurrencyLayerClientTest extends TestCase
{
    use DependencyMocking;

    /** @var CurrencyLayerClient */
    private $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = $this->constructWithMockedDependencies(
            CurrencyLayerClient::class,

            client::class,
            RateTransformer::class
        );
    }

    /**
     * @dataProvider getRatesDataProvider
     *
     * @param $source
     * @param $targets
     * @param $response
     * @param $expected
     * @param null $exception
     * @param null $transformerException
     *
     * @throws GetRatesFailedException
     */
    public function testGetRates(
        $source,
        $targets,
        $response,
        $expected,
        $exception = null,
        $transformerException = null
    ) {
        $this->getServiceDependencyMock(client::class)
            ->source(Argument::any())
            ->willReturn($this->getServiceDependency(client::class));

        $this->getServiceDependencyMock(client::class)
            ->currencies(Argument::any())
            ->willReturn($this->getServiceDependency(client::class));

        $this->getServiceDependencyMock(client::class)
            ->live(Argument::any())
            ->willReturn($response);

        $this->mockTransformer($transformerException);

        if ($exception) {
            $this->expectException($exception);
        }
        $actual = $this->client->getRates($source, $targets);
        $this->assertEquals($expected, $actual);
    }

    public function getRatesDataProvider()
    {
        yield 'Successful response' => [
            'EUR',
            [
                'USD',
                'CHF',
            ],
            [
                'success' => true,
                'source' => 'EUR',
                'quotes' => [
                    'EURUSD' => 1.123,
                    'EURCHF' => 1.321,
                ],
            ],
            [
                new Rate(new Currency('EUR'), new Currency('USD'), 1.123),
                new Rate(new Currency('EUR'), new Currency('CHF'), 1.321),
            ],
        ];

        yield 'Failed request' => [
            'EUR',
            [
                'USD',
                'CHF',
            ],
            [
                'success' => false,
            ],
            [],
            GetRatesFailedException::class,
        ];

        yield 'No quotes' => [
            'EUR',
            [
                'USD',
                'CHF',
            ],
            [
                'success' => true,
                'quotes' => [],
            ],
            [],
            GetRatesFailedException::class,
        ];

        yield 'Wrong source' => [
            'EUR',
            [
                'USD',
                'CHF',
            ],
            [
                'success' => true,
                'source' => 'EUR',
                'quotes' => [
                    'EURUSD' => 1.123,
                    'GBPCHF' => 1.321,
                ],
            ],
            [],
            InvalidArgumentException::class,
        ];

        yield 'Transformation exception' => [
            'EUR',
            [
                'USD',
                'CHF',
            ],
            [
                'success' => true,
                'source' => 'EUR',
                'quotes' => [
                    'EURUSD' => 1.123,
                    'GBPCHF' => 1.321,
                ],
            ],
            [],
            GetRatesFailedException::class,
            TransformFailedException::class,
        ];
    }

    private function mockTransformer($exception = null)
    {
        if ($exception) {
            $this->getServiceDependencyMock(RateTransformer::class)
                ->api2entity(Argument::type(ApiQuote::class))
                ->willThrow($exception);
        } else {
            $this->getServiceDependencyMock(RateTransformer::class)
                ->api2entity(Argument::type(ApiQuote::class))
                ->will(function ($args) {
                    $apiQuote = $args[0] ?? null;
                    Assert::that($apiQuote)->isInstanceOf(ApiQuote::class);
                    /** @var ApiQuote $apiQuote */
                    return new Rate(
                        new Currency(substr($apiQuote->getKey(), 0, 3)),
                        new Currency(substr($apiQuote->getKey(), 3, 6)),
                        $apiQuote->getQuote()
                    );
                });
        }
    }
}
